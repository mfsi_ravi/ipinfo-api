package com.mfsi.ipinfo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import com.mfsi.ipinfo.model.AppResponse;
import com.mfsi.ipinfo.model.UserProfileModel;
import com.mfsi.ipinfo.service.UserService;

@Controller
@RequestMapping("user")
public class UserController {

	@Autowired
	private UserService userService;

	@GetMapping("profile")
	public ResponseEntity<AppResponse<UserProfileModel>> getUserProfile() {
		AppResponse<UserProfileModel> response = new AppResponse<>();
		response.setData(userService.getCurrentUserProfile());
		return ResponseEntity.ok().body(response);
	}

	@PutMapping("profile")
	public ResponseEntity<AppResponse<UserProfileModel>> updateUserProfile(@RequestBody UserProfileModel profile) {
		AppResponse<UserProfileModel> response = new AppResponse<>();
		userService.updateUserProfile(profile);
		response.setData(profile);
		return ResponseEntity.ok().body(response);
	}
}
