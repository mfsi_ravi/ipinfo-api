package com.mfsi.ipinfo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import com.mfsi.ipinfo.model.AppResponse;
import com.mfsi.ipinfo.model.IpInfoModel;
import com.mfsi.ipinfo.service.IpInfoService;

@RestController
public class HomeController {

	@Autowired
	private IpInfoService infoService;

	@GetMapping("lookup/{ip}")
	public ResponseEntity<AppResponse<IpInfoModel>> getIpInfo(@PathVariable("ip") String ipAddress) {
		AppResponse<IpInfoModel> appResponse = new AppResponse<>();
		IpInfoModel ipInfo = infoService.getIpInfoByIp(ipAddress);
		appResponse.setData(ipInfo);
		return ResponseEntity.ok(appResponse);
	}
}
