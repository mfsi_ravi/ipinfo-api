package com.mfsi.ipinfo.controller;

import java.io.IOException;

import javax.servlet.http.HttpServletResponse;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import lombok.extern.slf4j.Slf4j;

@ControllerAdvice
@Slf4j
public class RestControllerErrorAdvice extends ResponseEntityExceptionHandler {

	@ExceptionHandler(Exception.class)
	public void catchOtherUnprocessedExceptions(Exception e, HttpServletResponse response) throws IOException {
		log.error("Unexpected application error: ", e);
		response.sendError(HttpStatus.INTERNAL_SERVER_ERROR.value(), "Error Message");
	}
}
