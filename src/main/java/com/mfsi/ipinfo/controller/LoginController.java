package com.mfsi.ipinfo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mfsi.ipinfo.model.AppResponse;
import com.mfsi.ipinfo.model.AuthenticationResponse;
import com.mfsi.ipinfo.model.UserModel;
import com.mfsi.ipinfo.security.AppUserPrincipal;
import com.mfsi.ipinfo.util.AuthUtils;

@RestController
@RequestMapping("login")
public class LoginController {

	@Autowired
	private AuthUtils authUtils;

	@PostMapping("")
	public ResponseEntity<AppResponse<AuthenticationResponse>> createAuthenticationToken(
			@RequestBody UserModel userModel) {
		String username = userModel.getUsername();
		String password = userModel.getPassword();
		AppResponse<AuthenticationResponse> response = new AppResponse<>();
		AppUserPrincipal userPrincipal = authUtils.userLogin(username, password);
		AuthenticationResponse auth = authUtils.authenticate(userPrincipal);

		response.setData(auth);
		return ResponseEntity.ok(response);
	}

}
