package com.mfsi.ipinfo.util;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

import com.mfsi.ipinfo.exception.IpInfoException;
import com.mfsi.ipinfo.model.UserModel;
import com.mfsi.ipinfo.security.AppUserPrincipal;

@Component
public class AppUtils {
	
	public UserModel getCurrentLoggedInUser() {
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		Object obj = authentication.getPrincipal();
		if (obj instanceof AppUserPrincipal) {
			AppUserPrincipal userPrincipal = (AppUserPrincipal) obj;
			return userPrincipal.getUserModel();
		}
		throw IpInfoException.getException("User not logged in");
	}

}
