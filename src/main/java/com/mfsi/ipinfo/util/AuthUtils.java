package com.mfsi.ipinfo.util;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.stereotype.Component;

import com.mfsi.ipinfo.exception.IpInfoException;
import com.mfsi.ipinfo.model.AuthenticationResponse;
import com.mfsi.ipinfo.security.AppUserDetailsService;
import com.mfsi.ipinfo.security.AppUserPrincipal;

@Component
public class AuthUtils {

	@Autowired
	private AuthenticationManager authenticationManager;

	@Autowired
	private JwtUtil jwtTokenUtil;

	@Autowired
	private AppUserDetailsService userDetailsService;

	void authenticate(String username, String password) {
		try {
			authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(username, password));
		} catch (DisabledException e) {
			throw new IpInfoException("User is disabled", e);
		} catch (BadCredentialsException e) {
			throw new IpInfoException("Incorrect login credentials", e);
		}
	}

	public AppUserPrincipal userLogin(String username, String password) {
		authenticate(username, password);

		return (AppUserPrincipal) userDetailsService.loadUserByUsername(username);
	}

	public AuthenticationResponse authenticate(AppUserPrincipal userDetails) {
		final String jwt = jwtTokenUtil.generateToken(userDetails);
		AuthenticationResponse auth = new AuthenticationResponse(jwt);
		auth.setLoginTime(LocalDateTime.now().format(DateTimeFormatter.ofPattern("dd-MMM-yyyy HH:mm:ss")));
		auth.setUsername(userDetails.getUsername());
		return auth;
	}
}
