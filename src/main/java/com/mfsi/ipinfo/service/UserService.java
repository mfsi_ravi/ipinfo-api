package com.mfsi.ipinfo.service;

import java.util.Optional;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mfsi.ipinfo.entity.UserEntity;
import com.mfsi.ipinfo.entity.UserProfileEntity;
import com.mfsi.ipinfo.exception.IpInfoException;
import com.mfsi.ipinfo.model.UserModel;
import com.mfsi.ipinfo.model.UserProfileModel;
import com.mfsi.ipinfo.repo.UserProfileRepo;
import com.mfsi.ipinfo.repo.UserRepo;
import com.mfsi.ipinfo.util.AppUtils;

@Service
public class UserService {

	@Autowired
	private UserRepo userRepo;

	@Autowired
	private UserProfileRepo profileRepo;

	@Autowired
	private AppUtils appUtils;

	/**
	 * 
	 * @param userName
	 * @return
	 */
	public UserEntity checkUserLogin(String userName) {
		return userRepo.findByUsername(userName).orElseThrow(() -> IpInfoException.getException("User not found!"));
	}

	public UserProfileModel getCurrentUserProfile() {
		UserModel user = appUtils.getCurrentLoggedInUser();
		Optional<UserProfileEntity> optProfile = profileRepo.findByUserKey(user.getUserKey());
		if (optProfile.isPresent()) {
			UserProfileModel profile = new UserProfileModel();
			BeanUtils.copyProperties(optProfile.get(), profile);
			return profile;
		}
		throw IpInfoException.getException("Profile not exists");
	}

	public void updateUserProfile(UserProfileModel updatedProfile) {
		UserModel user = appUtils.getCurrentLoggedInUser();
		String userKey = user.getUserKey();
		Optional<UserProfileEntity> optProfile = profileRepo.findByUserKey(userKey);
		if (optProfile.isPresent()) {
			UserProfileEntity currentProfile = optProfile.get();
			BeanUtils.copyProperties(updatedProfile, currentProfile, "creationTime", "profileId", "userKey");
			profileRepo.saveAndFlush(currentProfile);
		}

	}
}
