package com.mfsi.ipinfo.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import com.mfsi.ipinfo.model.IpInfoModel;

@Service
public class IpInfoService {

	@Autowired
	private RestTemplate restTemplate;

	@Value("${app.ipinfo.url}")
	private String ipInfoUrl;

	public IpInfoModel getIpInfoByIp(String ipAddress) {
		String requestUrl = ipInfoUrl + ipAddress + "/json";

		HttpHeaders headers = new HttpHeaders();
		headers.add(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE);
		HttpEntity<String> entity = new HttpEntity<>(headers);

		UriComponentsBuilder builder = UriComponentsBuilder.fromUriString(requestUrl);
		return restTemplate.exchange(builder.build().toUri(), HttpMethod.GET, entity, IpInfoModel.class).getBody();
	}
}
