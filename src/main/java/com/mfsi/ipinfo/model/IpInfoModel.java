package com.mfsi.ipinfo.model;

import lombok.Data;

@Data
public class IpInfoModel {
	private String ip;
	private String hostname;
	private String city;
	private String region;
	private String country;
	private String loc;
	private String org;
	private String postal;
	private String timezone;
}
