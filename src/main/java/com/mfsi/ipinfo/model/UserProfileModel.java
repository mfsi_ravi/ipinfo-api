package com.mfsi.ipinfo.model;

import java.time.LocalDate;
import java.time.LocalDateTime;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Data;

@Data
public class UserProfileModel {
	@JsonIgnore
	private String userKey;
	private String personName;
	private String phoneNo;
	private LocalDate dateOfBirth;
	private String email;
	private LocalDateTime creationTime;
}
