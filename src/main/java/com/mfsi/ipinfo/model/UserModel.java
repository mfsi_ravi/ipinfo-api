package com.mfsi.ipinfo.model;

import java.io.Serializable;
import java.time.LocalDateTime;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Data;

@Data
public class UserModel implements Serializable {
	private static final long serialVersionUID = 1359069779790410627L;

	private String username;
	private String password;
	private String jwt;
	private LocalDateTime loginTime;

	@JsonIgnore
	private String userKey;
}
