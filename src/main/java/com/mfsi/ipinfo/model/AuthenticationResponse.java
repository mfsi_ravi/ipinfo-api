package com.mfsi.ipinfo.model;

import java.io.Serializable;
import java.util.List;

import lombok.Data;

@Data
public class AuthenticationResponse implements Serializable {

	private static final long serialVersionUID = -949984777862344883L;

	private final String jwt;
	private int id;
	private String username;
	private String loginTime;
	private List<String> roles;

	public AuthenticationResponse(String jwt) {
		this.jwt = jwt;
	}
}
