package com.mfsi.ipinfo.security;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.mfsi.ipinfo.entity.UserEntity;
import com.mfsi.ipinfo.model.UserModel;
import com.mfsi.ipinfo.repo.UserRepo;

@Service
public class AppUserDetailsService implements UserDetailsService {

	@Autowired
	private UserRepo userRepo;

	@Override
	public UserDetails loadUserByUsername(String userName) {
		Optional<UserEntity> entities = userRepo.findByUsername(userName);
		if (entities.isPresent()) {
			UserEntity entity = entities.get();
			UserModel user = new UserModel();
			user.setUsername(entity.getUsername());
			user.setPassword(entity.getPassword());
			user.setUserKey(entity.getUserId());
			return new AppUserPrincipal(user);
		}
		throw new UsernameNotFoundException("User not found!");
	}
}
