package com.mfsi.ipinfo.entity;

import java.io.Serializable;
import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Data
@Entity(name = "UserLogin")
@Table(name = "UserLogin")
public class UserEntity implements Serializable {
	private static final long serialVersionUID = -6775293195759581901L;

	@Column(name = "UserName")
	private String username;
	
	@Column(name = "Password")
	private String password;
	
	@Id
	@Column(name = "UserId")
	private String userId;
	
	@Column(name = "CreationDate")
	private LocalDateTime creationDate;

}
