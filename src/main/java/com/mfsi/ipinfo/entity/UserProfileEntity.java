package com.mfsi.ipinfo.entity;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Data
@Entity(name = "UserProfile")
@Table(name = "UserProfile")
public class UserProfileEntity implements Serializable {
	/**
	* 
	*/
	private static final long serialVersionUID = 2712411230875853467L;

	@Id
	@Column(name = "ProfileId")
	private int profileId;

	@Column(name = "UserKey")
	private String userKey;

	@Column(name = "PersonName")
	private String personName;

	@Column(name = "PhoneNo")
	private String phoneNo;

	@Column(name = "DateOfBirth")
	private LocalDate dateOfBirth;

	@Column(name = "EmailId")
	private String email;

	@Column(name = "CreationTime")
	private LocalDateTime creationTime;
}
