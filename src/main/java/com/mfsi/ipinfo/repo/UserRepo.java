package com.mfsi.ipinfo.repo;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.mfsi.ipinfo.entity.UserEntity;

@Repository
public interface UserRepo extends JpaRepository<UserEntity, String> {
	Optional<UserEntity> findByUsername(String username);
}
