package com.mfsi.ipinfo.repo;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.mfsi.ipinfo.entity.UserProfileEntity;

@Repository
public interface UserProfileRepo extends JpaRepository<UserProfileEntity, Integer> {
	Optional<UserProfileEntity> findByUserKey(String userKey);
}
