package com.mfsi.ipinfo.exception;

import java.text.MessageFormat;

public class IpInfoException extends RuntimeException {

	private static final long serialVersionUID = -7815095047994778196L;

	public IpInfoException(Throwable e) {
		super(e);
	}

	public IpInfoException(String message, Throwable e) {
		super(message, e);
	}

	public IpInfoException(String message) {
		super(message);
	}

	public static IpInfoException getException(String message, Throwable e) {
		return new IpInfoException(message, e);
	}

	public static IpInfoException getException(String message) {
		return new IpInfoException(message);
	}

	public static IpInfoException getException(String pattern, Object... arguments) {
		return new IpInfoException(MessageFormat.format(pattern, arguments));
	}

	public static IpInfoException getException(Throwable e) {
		return new IpInfoException(e);
	}

}
