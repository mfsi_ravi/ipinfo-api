package com.mfsi.ipinfo;

import static org.assertj.core.api.Assertions.assertThat;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import com.mfsi.ipinfo.controller.UserController;
import com.mfsi.ipinfo.service.UserService;

@SpringBootTest
class IpInfoApisApplicationTests {

	@Autowired
	private UserController homeController;
	
	@Autowired
	private UserService userService;
	
	@Test
	void contextLoads() {
		assertThat(homeController).isNotNull();
		assertThat(userService).isNotNull();
	}

}
