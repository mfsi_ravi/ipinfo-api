package com.mfsi.ipinfo.service;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import com.mfsi.ipinfo.model.IpInfoModel;

@SpringBootTest
class IpInfoServiceTest {

	@Autowired
	private IpInfoService ipInfoService;

	@DisplayName("Test IP Address Information")
	@Test
	void testIpInfo() {
		String ip = "103.205.67.234";
		assertEquals(getIpModel(), ipInfoService.getIpInfoByIp(ip));
	}

	private IpInfoModel getIpModel() {
		IpInfoModel model = new IpInfoModel();
		model.setCity("Mumbai");
		model.setCountry("IN");
		model.setIp("103.205.67.234");
		model.setLoc("19.0728,72.8826");
		model.setOrg("AS17439 Netmagic Datacenter Mumbai");
		model.setPostal("400070");
		model.setRegion("Maharashtra");
		model.setTimezone("Asia/Kolkata");
		return model;
	}
}
